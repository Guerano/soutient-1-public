#ifndef TREE_H
# define TREE_H

/*
** STRUCT FOR BTREE + BSTREE
*/
typedef struct btree
{
    int value;
    struct btree *left_son;
    struct btree *right_son;
} s_btree;

s_btree *btree_create(int value);

s_btree *btree_add(s_btree *tree, int value);

s_btree *btree_delete(s_btree *tree, int value);

void btree_print_dot(s_btree *tree);

/*
** STRUCT FOR AVL
*/
typedef struct avl
{
    int value;
    int balance;
    struct avl *left_son;
    struct avl *right_son;
} s_avl;

#endif /* !TREE_H */
