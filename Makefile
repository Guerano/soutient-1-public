CC=clang
CFLAGS=-Wall -Wextra -Werror -pedantic -std=c99 -g3
SRC=btree.c
OBJ=$(SRC:.c=.o)
BIN=tree
PNG=btree.png
DOT=btree.dot

all:$(OBJ) $(BIN)

run:$(OBJ) $(BIN)
	./$(BIN)

$(BIN):$(OBJ)
	$(CC) $(CFLAGS) -o $@ $^

clean:
	rm -rf $(BIN) $(OBJ)
	rm -rf $(PNG) $(DOT)
	rm -rf *.sw?

print: run
	dot -Tpng $(DOT) > $(PNG) && eog $(PNG)
