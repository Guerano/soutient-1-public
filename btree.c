#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "tree.h"

/*
** TELL WHICH SON TO GO ON IN ORDER TO ADD A NODE IN BTREE
*/
static inline int son_left(void)
{
    return rand() % 2;
}

s_btree *btree_create(int value)
{
    s_btree *tree = calloc(1, sizeof (s_btree));
    tree->left_son = NULL;
    tree->right_son = NULL;
    tree->value = value;
    return tree;
}

s_btree *btree_add(s_btree *tree, int value)
{
    if (!tree)
        return btree_create(value);
    s_btree *cpy = tree;
    s_btree *new = btree_create(value);
    int left_son = son_left();
    s_btree *son = left_son ? tree->left_son : tree->right_son;
    while (son)
    {
        tree = son;
        left_son = son_left();
        son = left_son ? tree->left_son : tree->right_son;
    }

    if (left_son)
        tree->left_son = new;
    else
        tree->right_son = new;
    return cpy;
}

static s_btree *btree_find(s_btree *tree, int value)
{
    if (!tree)
        return NULL;
    if (tree->value == value)
        return tree;
    s_btree *tmp = btree_find(tree->left_son, value);
    if (tmp)
        return tmp;
    tmp = btree_find(tree->right_son, value);
    return tmp;
}

static inline void btree_swap(s_btree *node1, s_btree *node2)
{
    int tmp = node1->value;
    node1->value = node2->value;
    node2->value = tmp;

    // One line version
    //node2->value = node1->value + node2->value - (node1->value = node2->value);
}

s_btree *btree_delete(s_btree *tree, int value)
{
    s_btree *node = btree_find(tree, value);
    if (!node)
        return tree;
    s_btree *tmp = tree;
    s_btree *prec = tree;
    while (tmp->left_son != tmp->right_son)
    {
        if (tmp->left_son)
        {
            prec = tmp;
            tmp = tmp->left_son;
        }
        else if (tmp->right_son)
        {
            prec = tmp;
            tmp = tmp->right_son;
        }
    }
    btree_swap(node, tmp);
    if (prec->right_son == tmp)
    {
        free(tmp);
        prec->right_son = NULL;
    }
    else
    {
        free(tmp);
        prec->left_son = NULL;
    }
    return tree;
}

static void btree_print_dot_rec(s_btree *tree, FILE *f)
{
    if (tree->left_son)
    {
      fprintf(f, "    %d -> %d;\n", tree->value, tree->left_son->value);
      btree_print_dot_rec(tree->left_son, f);
    }

    if (tree->right_son)
    {
      fprintf(f, "    %d -> %d;\n", tree->value, tree->right_son->value);
      btree_print_dot_rec(tree->right_son, f);
    }
}

void btree_print_dot(s_btree *tree)
{
    if (!tree)
      return;

    FILE *out = fopen("btree.dot", "w");
    fprintf(out, "digraph tree {\n");
    btree_print_dot_rec(tree, out);
    fprintf(out, "}");
    fclose(out);
}

int main(void)
{
    srand(time(NULL));
    s_btree *tree = btree_create(42);
    tree = btree_add(tree, 1);
    tree = btree_add(tree, 2);
    tree = btree_add(tree, 3);
    tree = btree_add(tree, 4);
    tree = btree_add(tree, 5);
    tree = btree_add(tree, 6);
    tree = btree_add(tree, 7);
    tree = btree_add(tree, 8);
    tree = btree_add(tree, 9);
    tree = btree_add(tree, 10);
    tree = btree_add(tree, 11);
    tree = btree_delete(tree, 42);
    btree_print_dot(tree);
    return 0;
}
